package com.projeto.atividade.controllers;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

public class Pessoa {
    private String nome;
    private String hora;
    private int vlAleatorio;

    public Pessoa(){
        this.nome = nome;
        this.hora = hora;
        Random random = new Random();
        this.setVlAleatorio(random.nextInt(100));
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


    public String getHora() {
        return hora;
    }

    public void setHora(Date hora) {
        DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
        this.hora = dateFormat.format(hora);
    }

    public int getVlAleatorio() {
        return vlAleatorio;
    }

    public void setVlAleatorio(int vlAleatorio) {
        this.vlAleatorio = vlAleatorio;
    }

    @Override
    public String toString() {
        return "Nome: "+ getNome()
                +"\r\n </br> Hora: "+ getHora()
                +"\r\n </br> Número: "+ getVlAleatorio();
    }
}
